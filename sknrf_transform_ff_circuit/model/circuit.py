import os

import torch as th
from scipy import interpolate

from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.enums.device import Instrument
from sknrf.enums.signal import transform_color_map
from sknrf.settings import Settings
from sknrf.device.signal import ff
from sknrf.model.transform.ff.base import FFTransform
from sknrf.app.dataviewer.model.mdif import MDIF
from sknrf.utilities.rf import a4t, t4tp, a2v, v2a
from sknrf.utilities.rf import n2t
from sknrf.utilities.numeric import Domain


class TeeTransform(FFTransform):
    r"""Tee Circuit Transform

    ..  figure:: ../../_images/PNG/t_circuit_transform.png
        :width: 50 %
        :align: center

    .. math::
        \left[\begin{array}{c} v_{1} \\ i_{1} \\ v_{2} \\ i_{2} \end{array} \right]_{l} =
        \left[\begin{array}{cccc} 1 & -Z_3 & 0 & Z_3 \\
                                  0 & 1    & 0 & 0    \\
                                  0 & Z_3 & 1 & -Z_3 \\
                                  0 & 0    & 0 & 1    \\ \end{array} \right]
        \left[\begin{array}{cccc} 1 & -Z_1 & 0 & 0    \\
                                  0 & 1    & 0 & 0    \\
                                  0 & 0    & 1 & -Z_2 \\
                                  0 & 0    & 0 & 1 \\ \end{array} \right]_{l}
        \left[\begin{array}{c} v_{1}' \\ i_{1}' \\ v_{2}' \\ i_{2}' \end{array} \right]_{l}\\
        \left[\begin{array}{c} z_{1} \\ z_{2} \end{array} \right]_{l} =
        \left[\begin{array}{cc} 1         & 0 \\
                                0         & 1 \\ \end{array} \right]
        \left[\begin{array}{c} z_{1}' \\ z_{2}' \end{array} \right]_{l} +
        \left[\begin{array}{cc} Z_1 & 0 \\
                                0         & Z_2 \\ \end{array} \right]

    A tee-circuit transform on port 1 and 2 of a 2-port system.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    filename : str
        Filename of (.s4p) S-parameter file that contains the transform coefficients.

    """

    _num_ports = 2
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/t_circuit_transform.png"
    _default_filename = "Tee_Thru.mdf"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "file"]
    optimize = False

    def __new__(cls, name: str = "Tee", ports: tuple = (1, 2), instrument_flags=Instrument.RF,
                z: th.Tensor = th.zeros((3,), dtype=si_dtype_map[SI.Z]), filename: str = ""):
        self = super(TeeTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                data=z)
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()
        self._z = z
        return self

    def __getnewargs__(self):
        return tuple(list(super(TeeTransform, self).__getnewargs__()) +
                     [self._z, self._filename])

    def __init__(self, name: str = "Tee", ports: tuple = (1, 2), instrument_flags=Instrument.RF,
                 z: th.Tensor = th.zeros((3,), dtype=si_dtype_map[SI.Z]), filename: str = ""):
        super(TeeTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                           data=z)
        self.__info__()
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()

    def __info__(self):
        super(TeeTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file):
        with open(file.name, mode='r') as f:
            freq = ff.freq()
            mdif = MDIF.read(f.name)
            mdif_freq = mdif.dataBlocks[0].data[0].independent.real

            mdif_dep = mdif.dataBlocks[0].data[0].dependents[:, :]
            z = th.zeros((freq.shape[0], mdif_dep.shape[1]), dtype=si_dtype_map[SI.Z])
            freq[freq < mdif_freq[-0]] = mdif_freq[0]
            freq[freq > mdif_freq[-1]] = mdif_freq[-1]
            for z_index in range(0, mdif_dep.shape[1]):
                freq_interp = interpolate.interp1d(mdif_freq, mdif_dep[:, z_index], kind="linear", assume_sorted=True)
                z[:, z_index] = n2t(freq_interp(freq))
            z = z.reshape(Settings().f_points, Settings().t_points, 3)
            z = z.permute(-2, -3, -1)
            z = z.reshape((-1, 3))
            self._set_data(z)
            self._filename = f.name
            self._file = f

    def _data(self):
        return self._data_

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1) if data.dim() == 1 else data
        self._z = data
        abcd = th.eye(4, dtype=self._z.dtype).repeat(self._z.shape[0], 1, 1)
        abcd[..., 0, 1] = -1*(self._z[:, 0] + self._z[:, 2])
        abcd[..., 0, 3] = abcd[:, 2, 1] = self._z[:, 2]
        abcd[..., 2, 3] = -1*(self._z[:, 1] + self._z[:, 2])
        super(TeeTransform, self)._set_data(abcd)

    def get_stimulus(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_p_1, z_p_2 = z_1, z_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z_1, z_2 = z_1.reshape(shape), z_2.reshape(shape)
            z_p_1 = z_1 + self._z[:, 0]
            z_p_2 = z_2 + self._z[:, 1]

        if self.instrument_flags & Instrument.STIMULUS:
            tp = t4tp(a4t(self._abcd)[0], z_1.flatten(), z_2.flatten(), z_p_1.flatten(), z_p_2.flatten())[0]
            tp[0::Settings().f_points, :, :] = th.eye(4, dtype=tp.dtype)  # Force LF to be stable
            gain = tp[..., 1::2, 1::2]
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, z_1, z_p_1 = v_1.reshape(shape), z_1.reshape(shape), z_p_1.reshape(shape)
            v_2, z_2, z_p_2 = v_2.reshape(shape), z_2.reshape(shape), z_p_2.reshape(shape)
            v_1_2 = th.cat((v_1, v_2), dim=-2)
            z_1_2 = th.cat((z_1, z_2), dim=-2)
            z_p_1_2 = th.cat((z_p_1, z_p_2), dim=-2)
            v_1_2 = a2v(th.matmul(gain, v2a(v_1_2, z_1_2)[0]), z_p_1_2)[0]
            v_1, v_2 = th.split(v_1_2, 1, dim=-2)
            z_p_1, z_p_2 = th.split(z_p_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_p_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_p_2.reshape(s)
        return tuple(res)

    def set_stimulus(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_p_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_p_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_1, z_2 = z_p_1, z_p_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z_p_1, z_p_2 = z_p_1.reshape(shape), z_p_2.reshape(shape)
            z_1 = z_p_1 - self._z[:, 0]
            z_2 = z_p_2 - self._z[:, 1]

        if self.instrument_flags & Instrument.STIMULUS:
            tp = t4tp(a4t(self._abcd)[0], z_1.flatten(), z_2.flatten(), z_p_1.flatten(), z_p_2.flatten())[0]
            tp[0::Settings().f_points, :, :] = th.eye(4, dtype=tp.dtype)  # Force LF to be stable
            gain_inv = th.inverse(tp[..., 1::2, 1::2])
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, z_p_1, z_1 = v_1.reshape(shape), z_p_1.reshape(shape), z_1.reshape(shape)
            v_2, z_p_2, z_2 = v_2.reshape(shape), z_p_2.reshape(shape), z_2.reshape(shape)
            v_1_2 = th.cat((v_1, v_2), dim=-2)
            z_p_1_2 = th.cat((z_p_1, z_p_2), dim=-2)
            z_1_2 = th.cat((z_1, z_2), dim=-2)
            v_1_2 = a2v(th.matmul(gain_inv, v2a(v_1_2, z_p_1_2)[0]), z_1_2)[0]
            v_1, v_2 = th.split(v_1_2, 1, dim=-2)
            z_1, z_2 = th.split(z_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_2.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_p_1, z_p_2 = z_1, z_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z_1, z_2 = z_1.reshape(shape), z_2.reshape(shape)
            z_p_1 = z_1 + self._z[:, 0]
            z_p_2 = z_2 + self._z[:, 1]

        if self.instrument_flags & Instrument.STIMULUS:
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, i_1 = v_1.reshape(shape), i_1.reshape(shape)
            v_2, i_2 = v_2.reshape(shape), i_2.reshape(shape)
            vi_1_2 = th.cat((v_1, i_1, v_2, i_2), dim=-2)
            vi_1_2 = th.matmul(self._abcd, vi_1_2)
            v_1, i_1, v_2, i_2 = th.split(vi_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_p_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_p_2.reshape(s)
        return tuple(res)


class PiTransform(FFTransform):
    r"""Pi Circuit Transform

    ..  figure:: ../../_images/PNG/pi_circuit_transform.png
        :width: 50 %
        :align: center

    .. math::
        \left[\begin{array}{c} v_{1} \\ i_{1} \\ v_{2} \\ i_{2} \end{array} \right]_{l} =
        \left[\begin{array}{cccc} 1    & 0 & 0    & 0 \\
                                  -Y_3 & 1 & Y_3  & 0 \\
                                  0    & 0 & 1    & 0 \\
                                  Y_3 & 0 & -Y_3  & 1 \\ \end{array} \right]
        \left[\begin{array}{cccc} 1    & 0 & 0    & 0 \\
                                  -Y_1 & 1 & 0    & 0 \\
                                  0    & 0 & 1    & 0 \\
                                  0    & 0 & -Y_2 & 1 \\ \end{array} \right]_{l}
        \left[\begin{array}{c} v_{1}' \\ i_{1}' \\ v_{2}' \\ i_{2}' \end{array} \right]_{l} \\
        \left[\begin{array}{c} y_{1} \\ y_{2} \end{array} \right]_{l} =
        \left[\begin{array}{cc} 1         & 0 \\
                                0         & 1 \\ \end{array} \right]
        \left[\begin{array}{c} y_{1}' \\ y_{2}' \end{array} \right]_{l} +
        \left[\begin{array}{cc} Y_1 & 0 \\
                                0         & Y_2 \\ \end{array} \right]

    A pi-circuit transform on port 1 and 2 of a 2-port system.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    filename : str
        Filename of (.s4p) S-parameter file that contains the transform coefficients.

    """

    _num_ports = 2
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/pi_circuit_transform.png"
    _default_filename = "Pi_Thru.mdf"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "file"]
    optimize = False

    def __new__(cls, name: str = "Pi", ports: tuple = (1, 2), instrument_flags=Instrument.RF,
                y: th.Tensor = th.zeros((3,), dtype=si_dtype_map[SI.Z]), filename: str = ""):
        self = super(PiTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                               data=y)
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()
        self._y = y
        return self

    def __getnewargs__(self):
        return tuple(list(super(PiTransform, self).__getnewargs__()) +
                     [self._y, self._filename])

    def __init__(self, name: str = "Pi", ports: tuple = (1, 2), instrument_flags=Instrument.RF,
                 y: th.Tensor = th.zeros((3,), dtype=si_dtype_map[SI.Z]), filename: str = ""):
        super(PiTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                          data=y)
        self.__info__()
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()

    def __info__(self):
        super(PiTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file):
        with open(file.name, mode='r') as f:
            freq = ff.freq()
            mdif = MDIF.read(f.name)
            mdif_freq = mdif.dataBlocks[0].data[0].independent.real

            mdif_dep = mdif.dataBlocks[0].data[0].dependents[:, :]
            y = th.zeros((freq.shape[0], mdif_dep.shape[1]), dtype=si_dtype_map[SI.Z])
            freq[freq < mdif_freq[-0]] = mdif_freq[0]
            freq[freq > mdif_freq[-1]] = mdif_freq[-1]
            for y_index in range(0, mdif_dep.shape[1]):
                freq_interp = interpolate.interp1d(mdif_freq, mdif_dep[:, y_index], kind="linear", assume_sorted=True)
                y[:, y_index] = n2t(freq_interp(freq))
            y = y.reshape(Settings().f_points, Settings().t_points, 3)
            y = y.permute(-2, -3, -1)
            y = y.reshape(-1, 3)
            self._set_data(y)
            self._filename = f.name
            self._file = f

    def _data(self):
        return self._data_

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1) if data.dim() == 1 else data
        self._y = data
        abcd = th.eye(4, dtype=self._y.dtype).repeat(self._y.shape[0], 1, 1)
        abcd[:, 1, 0] = -1*(self._y[:, 0] + self._y[:, 2])
        abcd[:, 1, 2] = abcd[:, 3, 0] = self._y[:, 2]
        abcd[:, 3, 2] = -1*(self._y[:, 1] + self._y[:, 2])
        super(PiTransform, self)._set_data(abcd)

    def get_stimulus(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_p_1, z_p_2 = z_1, z_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            y_1, y_2 = 1/z_1.reshape(shape), 1/z_2.reshape(shape)
            z_p_1 = 1/(y_1 + self._y[:, 0])
            z_p_2 = 1/(y_2 + self._y[:, 1])

        if self.instrument_flags & Instrument.STIMULUS:
            tp = t4tp(a4t(self._abcd)[0], z_1.flatten(), z_2.flatten(), z_p_1.flatten(), z_p_2.flatten())[0]
            tp[0::Settings().f_points, :, :] = th.eye(4, dtype=tp.dtype)  # Force LF to be stable
            gain = tp[:, 1::2, 1::2]
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, z_1, z_p_1 = v_1.reshape(shape), z_1.reshape(shape), z_p_1.reshape(shape)
            v_2, z_2, z_p_2 = v_2.reshape(shape), z_2.reshape(shape), z_p_2.reshape(shape)
            v_1_2 = th.cat((v_1, v_2), dim=-2)
            z_1_2 = th.cat((z_1, z_2), dim=-2)
            z_p_1_2 = th.cat((z_p_1, z_p_2), dim=-2)
            v_1_2 = a2v(th.matmul(gain, v2a(v_1_2, z_1_2)[0]), z_p_1_2)[0]
            v_1, v_2 = th.split(v_1_2, 1, dim=-2)
            z_p_1, z_p_2 = th.split(z_p_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_p_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_p_2.reshape(s)
        return tuple(res)

    def set_stimulus(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_p_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_p_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_1, z_2 = z_p_1, z_p_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            y_p_1, y_p_2 = 1/z_1.reshape(shape), 1/z_2.reshape(shape)
            z_1 = 1/(y_p_1 - self._y[:, 0])
            z_2 = 1/(y_p_2 - self._y[:, 1])

        if self.instrument_flags & Instrument.STIMULUS:
            pass
            tp = t4tp(a4t(self._abcd)[0], z_1.flatten(), z_2.flatten(), z_p_1.flatten(), z_p_2.flatten())[0]
            tp[0::Settings().f_points, :, :] = th.eye(4, dtype=tp.dtype)  # Force LF to be stable
            gain_inv = th.inverse(tp[:, 1::2, 1::2])
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, z_p_1, z_1 = v_1.reshape(shape), z_p_1.reshape(shape), z_1.reshape(shape)
            v_2, z_p_2, z_2 = v_2.reshape(shape), z_p_2.reshape(shape), z_2.reshape(shape)
            v_1_2 = th.cat((v_1, v_2), dim=-2)
            z_p_1_2 = th.cat((z_p_1, z_p_2), dim=-2)
            z_1_2 = th.cat((z_1, z_2), dim=-2)
            v_1_2 = a2v(th.matmul(gain_inv, v2a(v_1_2, z_p_1_2)[0]), z_1_2)[0]
            v_1, v_2 = th.split(v_1_2, 1, dim=-2)
            z_1, z_2 = th.split(z_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_2.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        p_1, p_2 = self.ports
        v_1, i_1, z_1 = args[p_1][0], args[p_1][1], args[p_1][2]
        v_2, i_2, z_2 = args[p_2][0], args[p_2][1], args[p_2][2]
        s = v_1.shape

        z_p_1, z_p_2 = z_1, z_2
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            y_1, y_2 = 1/z_1.reshape(shape), 1/z_2.reshape(shape)
            z_p_1 = 1/(y_1 + self._y[:, 0])
            z_p_2 = 1/(y_2 + self._y[:, 1])

        if self.instrument_flags & Instrument.RESPONSE:
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v_1, i_1 = v_1.reshape(shape), i_1.reshape(shape)
            v_2, i_2 = v_2.reshape(shape), i_2.reshape(shape)
            vi_1_2 = th.cat((v_1, i_1, v_2, i_2), dim=-2)
            vi_1_2 = th.matmul(self._abcd, vi_1_2)
            v_1, i_1, v_2, i_2 = th.split(vi_1_2, 1, dim=-2)

        res = list(args)
        res[p_1][0], res[p_1][1], res[p_1][2] = v_1.reshape(s), i_1.reshape(s), z_p_1.reshape(s)
        res[p_2][0], res[p_2][1], res[p_2][2] = v_2.reshape(s), i_2.reshape(s), z_p_2.reshape(s)
        return tuple(res)
